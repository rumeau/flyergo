<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

Route::get('/', 'HomeController@home');
Route::post('contact', 'ContactController@send')
    ->name('contact');
Route::post('product-contact', 'ProductController@contact')
    ->name('contact.product');

Route::get('/why', 'PageController@why')->name('page.why');
Route::get('/hello', 'PageController@hello')->name('page.hello');

Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
], function() {
    Route::get('/', 'HomeController@home')
        ->name('home');

    // Localization
    Route::get('/js/lang.js', function () {
        //$strings = Cache::rememberForever('lang.js', function () {
        $lang = LaravelLocalization::getCurrentLocale();

        $files   = glob(resource_path('lang/' . $lang . '/*.php'));
        $strings = [];

        foreach ($files as $file) {
            $name           = basename($file, '.php');
            $strings[$name] = require $file;
        }

        //return $strings;
        //});

        header('Content-Type: text/javascript');
        echo('window.i18n = ' . json_encode($strings) . ';');
        exit();
    })->name('assets.lang');
});
