<?php
/**
 * Created by PhpStorm.
 * User: rumea
 * Date: 06/05/2018
 * Time: 22:18
 */

return [
    'afiches' => [
        'price' => '59',
        'img' => 'afiches.jpg',
        'order' => 2,
    ],

    'autocollants' => [
        'price' => '59',
        'img' => 'autocollants.jpg',
        'order' => 5,
    ],

    'baches' => [
        'price' => '49',
        'img' => 'baches.jpg',
        'order' => 7,
    ],

    'cartes_postal' => [
        'price' => '49',
        'img' => 'cartes_postal.jpg',
        'order' => 3,
    ],

    'cartes_visite' => [
        'price' => '49',
        'img' => 'cartes_visite.jpg',
        'order' => 4,
    ],

    'depliants' => [
        'price' => '49',
        'img' => 'depliants.jpg',
        'order' => 6,
    ],

    'flyers'  => [
        'price' => '49',
        'img' => 'flyers.jpg',
        'order' => 1,
    ],

    'lettrage' => [
        'price' => '59',
        'img' => 'lettrage.jpg',
        'order' => 8,
    ],

    'tickets' => [
        'price' => '49',
        'img' => 'tickets.jpg',
        'order' => 9,
    ],
];