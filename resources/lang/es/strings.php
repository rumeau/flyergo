<?php
/**
 * Created by PhpStorm.
 * User: rumea
 * Date: 06/05/2018
 * Time: 22:13
 */

return [
    'phone_number' => '+33 6 95 85 85 40',

    'products' => [
        'afiches' => 'Afiches',
        'autocollants' => 'Autocollants',
        'baches' => 'Bâches',
        'cartes_postal' => 'Cartes Postales',
        'cartes_visite' => 'Cartes Visite',
        'depliants' => 'Dépliants',
        'flyers' => 'Flyers',
        'lettrage' => 'Lettrage Adhésif',
        'tickets' => "Tickets d\'entrée",
    ],

    'product' => [
        'price_string' => 'Desde :price &euro;',
        'button' => 'Faites un Devis',
    ],

    'banner' => [
        'why' => [
            'title' => 'Pourquoi choisir',
            'subtitle' => 'Nous sommes là pour vous aider à chaque étape!',
            'button' => 'Contacto',
        ],
        'hello' => [
            'title' => 'Hola!',
            'subtitle' => 'Envoyez nous vos questions et commentaires go@flyergo.fr ',
            'button' => 'Contacto',
        ],
    ],

    'page' => [
        'hello' => [
            'body' => 'Notre principal point de contact est le courrier électronique, et nous pensons que nous y 
parvenons.<br>Pour les questions et demandes générales:<br><span class="hilite">info@flyergo.fr</span>'
        ],

        'why' => [
            'section-1' => [
                'title' => 'Bienvenue sur FlyerGo.fr',
                'subtitle' => 'De la sélection de vos produits imprimés à la conception parfaite de votre œuvre. 
                Avec nous, commander des documents imprimés ne devrait pas être difficile!'
            ],
            'section-2' => [
                'title' => 'Notre passion',
                'subtitle' => 'Nous faisons notre travail avec passion. Une relation client efficace figure en tête 
                de nos priorités et nous mettons tout en œuvre pour que le processus de commande se déroule le mieux 
                possible.'
            ],
            'section-3' => [
                'title' => 'l\'impression plus facile',
                'subtitle' => 'Chez flyergo, nous croyons en la qualité et, tout aussi important, en facilitant la 
                commande de documents imprimés en ligne pour nos clients.'
            ],
        ]
    ],

    'contactform' => [
        'name' => 'Nombre',
        'email' => 'E-mail',
        'company' => 'Empresa',
        'phone' => 'Teléfono',
        'address' => 'Dirección',
        'message' => 'Mensaje',
        'send' => 'Enviar',

        'sending' => 'Enviando...',
        'sent' => 'Enviado!',
    ]
];
