@extends ('layout.app')

@section ('hero')

    <div id="hero">
        <div class="container">
            <a href="{{ url('/') }}">
                <img src="{{ asset('img/hero.jpg') }}" alt="">
            </a>
        </div>
    </div>

@endsection

@section('banners')

    <div id="banners">
        <div class="container">
            <div class="banner">
                <img src="{{ asset('img/banner-why.jpg') }}" alt="">
                <div class="caption">
                    <p class="title">@lang('strings.banner.why.title') <span class="logo-small"></span></p>
                    <p class="subtitle">@lang('strings.banner.why.subtitle')</p>
                    <a href="{{ route('page.why') }}" class="button">@lang('strings.banner.why.button')</a>
                </div>
            </div>

            <div class="banner">
                <img src="{{ asset('img/banner-hello.jpg') }}" alt="">
                <div class="caption">
                    <p class="title">@lang('strings.banner.hello.title') <span class="logo-small"></span></p>
                    <p class="subtitle">@lang('strings.banner.hello.subtitle')</p>
                    <a href="{{ route('page.hello') }}" class="button">@lang('strings.banner.hello.button')</a>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('logos')
    <div id="logos">
        <div class="container">
            <div class="grid">
                <div class="column">
                    <img src="img/content/logos/logo_4.jpg" alt="">
                </div>
                <div class="column">
                    <img src="img/content/logos/logo_3.jpg" alt="">
                </div>
                <div class="column">
                    <img src="img/content/logos/logo_2.jpg" alt="">
                </div>
                <div class="column">
                    <img src="img/content/logos/logo_1.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
@endsection

@section ('content')

    @yield('hero')

    <div id="main">
        <div class="container">
            <div class="grid">

                @foreach ($products as $name => $product)
                    <div class="column" id="product-{{ $name }}">
                        <div class="item item-{{ $name }}">
                            <div class="item-img">
                                <img src="{{ asset('img/content/' . $product['img']) }}" alt="{{ trans('strings.products.' . $name) }}">
                            </div>
                            <div class="item-info">
                                <h2>{{ trans('strings.products.' . $name) }}</h2>
                                <span>{!! trans('strings.product.price_string', ['price' => $product['price']])  !!}</span>

                                <product-form product="{{ $name }}"></product-form>
                            </div>
                        </div>
                    </div>

                @endforeach
            </div>
        </div>
    </div>

    @yield('banners')

    @yield('logos')

@endsection