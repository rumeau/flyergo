<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.title') }}</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>

<body>

<div class="wrap" id="app">


    <header id="header">
        <div class="container">
            <h1 class="logo">
                <a href="{{ url('/') }}">{{ config('app.title') }}</a>
            </h1>

            <nav class="utilmenu">
                <ul>
                    <li class="whatsapp">
                        {{ trans('strings.phone_number') }}
                    </li>
                    <li class="lang left {{ App::getLocale() === 'fr' ? 'active' : '' }}">
                        <a href="{{ url('/fr') }}">fr</a>
                    </li>
                    <li class="lang right {{ App::getLocale() === 'es' ? 'active' : '' }}">
                        <a href="{{ url('/es') }}">es</a>
                    </li>
                </ul>
            </nav>

            <nav class="mainmenu">
                <ul>
                    <li>
                        <a href="#">accueil</a>
                    </li>
                    <li>
                        <a href="#">produits</a>
                    </li>
                    <li>
                        <a href="#">a propous de nous</a>
                    </li>
                    <li>
                        <a href="#">promo</a>
                    </li>
                    <li>
                        <a href="#">contact</a>
                    </li>
                </ul>
            </nav>
        </div>

    </header>

    @yield('content')

    <footer id="footer">
        <div class="container">

            <div id="newsletter">

                <form action="">
                    <p>Interessé par notre Newsletter?</p>

                    <div class="input-row">
                        <input type="text" placeholder="Votre adresse e-mail">
                        <input type="submit" value="Oui, je m´inscris">
                    </div>
                </form>

            </div>

            <div id="foot">
                <div class="col">
                    <p class="title">About Us</p>
                    <p>We're a small 	studio of design 	located in
                        <br>
                        Toulouse with 	customers all over the world.<br>
                        We've been printing your flyers since 2017
                        <br>
                        and having fun while we do it!
                    </p>
                    <p class="title">Customers Service</p>
                    <p>Questions? Concerns?
                        <br>
                        You can email us anytime at go@flyergo.fr
                        <br>
                        SIRET: 81887360600011
                    </p>
                    <p class="socials">
                    <ul class="icons">
                        <li class="icon facebook">
                            <a href="#">Facebook</a>
                        </li>
                        <li class="icon instagram">
                            <a href="#">Instagram</a>
                        </li>
                        <li class="icon twitter">
                            <a href="">Twitter</a>
                        </li>
                        <li class="icon whatsapp">
                            <a href="#" rel="nofollow">Whatsapp</a>
                        </li>
                        <li class="text"><span>+33 6 95 85 85 40</span></li>
                    </ul>
                    </p>
                </div>

                <div class="col">
                    <div class="contact">
                        <form-contact url="{{ route('contact') }}"></form-contact>
                    </div>
                </div>
            </div>

        </div>
    </footer>
</div>

<script src="{{ url('/js/lang.js') }}"></script>
<script src="{{ url('/js/app.js') }}"></script>

</body>
</html>
