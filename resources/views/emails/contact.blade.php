<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<p>
    Name: <strong>{{ $name }}</strong>
</p>
<p>
    Email: <strong>{{ $email }}</strong>
</p>
<p>
    Company: <strong>{{ $company }}</strong>
</p>
<p>
    Phone: <strong>{{ $phone }}</strong>
</p>
<p>
    Address: <strong>{{ $address }}</strong>
</p>
<p>
    Message: {{ $message }}
</p>

</body>
</html>