@extends('layout.app')

@section('content')

    <div id="main" class="page-hello">
        <div class="orange-bar"></div>

        <div class="container">

            <p>{!! trans('strings.page.hello.body') !!}</p>

        </div>
    </div>

@endsection