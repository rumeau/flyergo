@extends('layout.app')

@section('content')

    <div id="main" class="page-why">
        <div class="orange-bar"></div>

        <div class="container section-1">

            <div class="container-inner">
                <div class="card">
                    <div class="card-inner">
                        <p class="title">@lang('strings.page.why.section-1.title')</p>
                        <p class="subtitle">@lang('strings.page.why.section-1.subtitle')</p>
                    </div>
                </div>
            </div>

        </div>

        <div class="container section-2">

            <div class="container-inner">
                <div class="card">
                    <div class="card-inner">
                        <p class="title">@lang('strings.page.why.section-2.title')</p>
                        <p class="subtitle">@lang('strings.page.why.section-2.subtitle')</p>
                    </div>
                </div>
            </div>

        </div>

        <div class="container section-3">

            <div class="container-inner">
                <div class="card">
                    <div class="card-inner">
                        <p class="title">@lang('strings.page.why.section-3.title')</p>
                        <p class="subtitle">@lang('strings.page.why.section-3.subtitle')</p>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection