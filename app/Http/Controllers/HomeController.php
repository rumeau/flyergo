<?php

namespace App\Http\Controllers;

class HomeController extends Controller
{
    public function home()
    {
        $products = collect(config('products', []))->sortBy('order');

        return view('home', compact('products'));
    }
}
