<?php
/**
 * Created by PhpStorm.
 * User: Jean
 * Date: 20-07-2018
 * Time: 2:10
 */

namespace App\Http\Controllers;


use App\Mail\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function send(Request $request)
    {
        $this->validate($request, $this->rules());

        Mail::to([config('mail.to.address') => config('mail.to.name')])
            ->send(new Contact($request->only('name', 'email', 'company', 'phone', 'address', 'message')));

        return response()->json([
            'status' => 'OK',
        ]);
    }

    /**
     * @return array
     */
    protected function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email',
        ];
    }
}