<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function why(Request $request)
    {
        return view('pages.why');
    }

    public function hello(Request $request)
    {
        return view('pages.hello');
    }
}
